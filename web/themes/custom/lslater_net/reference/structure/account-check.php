<?php

/*

- If using an admin account on the backend, redirect page on logged out state to a login page, and send the original url as a variable to redirect to after login 

- If on the main site, simply show logged out in the account section

*/

session_start();

if (isset($_SESSION['account'])) {
	if (!isset($_SESSION['account-time']) || time() - $_SESSION['account-time'] > 300) {
		/* Logged out */
		$login = false;
		$_SESSION['logged-in'] = false;
	} else {
		/* Logged in */
		$login = true;
		$_SESSION['logged-in'] = true;
	}
} else {
	/* Logged out */
	$login = false;
	$_SESSION['logged-in'] = false;
}

if ($login==true) {
	$_SESSION['account-time'] = time();
}

?>

