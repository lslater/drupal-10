<!-- head.php -->
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="icon" href="/includes/resources/images/tux-logo.png" type="image/gif" sizes="16x16">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="<?php echo isset($description)?$description:"Personal website of Lee Slater."; ?>" />

<?php require_once(dirname(__DIR__).'/constants.php'); ?>
<?php require_once('account-check.php'); ?>
<title><?php echo (isset($title)?$title." - ":"").$siteName; ?></title>

<?php require_once('style.php'); ?>

