<!-- layout1.php -->
	<body>
		<script>
			// Close the dropdown menu if the user clicks outside of it
			// TODO: Make this work with multiple dropdowns
			/*$(function() {
				$("body").click(function(e) {
					if (!(e.target.id == "nav" || $(e.target).parents("#nav").length)) {
						document.querySelector('#checkbox-toggle').checked = false;
					}
				});
			})*/
		</script>
		<header id="header">
			<a tabindex=1 href="/"><img id="logo" src="/includes/resources/images/tux-logo.png" alt="Tux Logo"></a>
			<div class="right-menu-container">
				<!-- Use a noscript tag for this, and set a JavaScript version for keyboard accessibility -->
				<input tabindex=3 type='checkbox' id='right-menu-toggle'>
				<label id="burger-menu" for='right-menu-toggle'>
					<div></div>
					<div></div>
					<div></div>
				</label>
				<div class="right-menu">
						<?php
						if ($_SESSION['logged-in']==true) {
							#echo "<span class='account-heading'>Welcome, Username</span>";
							#echo "<span><a href='/account/'>Edit account</a></span>";
							#echo "<span><a href='/account/'>Logout</a></span>";
						} else {
							echo "<div id='account-section'>";
							echo "<label for='account-toggle'>You are not logged in<span style='margin-left: 3px;' class='fa fa-angle-down'></span></label>";
							echo "<input type='checkbox' id='account-toggle'>";
							echo "<input type='text' placeholder='Username' />";
							echo "<input type='password' placeholder='Password' />";
							echo "<span><input type='button' class='button' value='Login'/><a id='register' href='/account/register/'>Register an account</a></span>";
							echo "</div>";
						}
						?>
					<nav>
						<ul id="nav" class="navbar">
						<?php
						foreach($navButtons as $key => $value) {
							if (!is_array($value)) {
								echo "<li class='nav-button'><a class='nav-link' href='".$value."'>".$key."</a></li>";
							} else {
								echo "<li class='nav-dropdown'>";
								echo "<input type='checkbox' id='checkbox-toggle'>";
								echo "<label for='checkbox-toggle'>$key<span style='margin-left: 3px;' class='fa fa-angle-down'></span></label>";
								echo "<ul>";
								foreach($value as $key1 => $value1) {
									echo "<li><a class='dropdown-button' href='$value1'>$key1</a></li>";
								}
								echo "</ul>";
								echo "</li>";
							}
						}
						?>
						</ul>
					</nav>
				</div>
			</div>
			<?php if ($search_bar!==false) { ?>
			<form class='search-form'>
				<input tabindex=2 class='search-bar' type="text" placeholder="Search...">
				<input tabindex=2 id="search-button" type="button"/>
			</form>
			<?php } ?>
			<!--<div class='nav-hide-container'>
				<input type='checkbox' id='menu-toggle'><label for='menu-toggle'></label>-->
				<!--<div class="nav-hide">-->
				<!--
				<nav class="nav-hide">
					<ul id="nav" class="navbar">-->
					<?php
					/*foreach($navButtons as $key => $value) {
						if (!is_array($value)) {
							echo "<li class='nav-button'><a class='nav-link' href='".$value."'>".$key."</a></li>";
						} else {
							echo "<li class='nav-dropdown'>";
							echo "<input type='checkbox' id='checkbox-toggle'>";
							echo "<label for='checkbox-toggle'>$key<span style='margin-left: 3px;' class='fa fa-angle-down'></span></label>";
							echo "<ul>";
							foreach($value as $key1 => $value1) {
								echo "<li><a class='dropdown-button' href='$value1'>$key1</a></li>";
							}
							echo "</ul>";
							echo "</li>";
						}
					}*/
					?>
					<!--</ul>
				</nav>-->
			<!--</div>-->
			<!--</div>-->
		</header>
		<div class="container">
			<div class="footer-buffer"></div>
			<main class="content">
		<?php
			if (!isset($breadcrumbArray)) {
				$breadcrumbVar = $_SERVER['REQUEST_URI'];
				$breadcrumbVar = ltrim($breadcrumbVar, "/");
				$breadcrumbVar = rtrim($breadcrumbVar, "/");
				$tmpBreadcrumbArray = explode("/", $breadcrumbVar);
				$breadcrumbArray = array();
				if ($_SERVER['REQUEST_URI']!='/') {
					$breadcrumbArray[] = array('Home'=>'/');
				}
				for ($i=0; $i<count($tmpBreadcrumbArray); $i++) {
					$link = "";
					for ($u=0; $u<=$i; $u++) {
						$link.="/".$tmpBreadcrumbArray[$u];
					}
					$breadcrumbArray[] = array($tmpBreadcrumbArray[$i]=>$link);
				}
			}
			//$breadcrumbArray = array(array('Categories'=>'/categories/'), array('General'=>'/categories/general/'))
		// print_r($breadcrumbArray);
		?>
		<div class="breadcrumb">
		<?php
			for ($i=0; $i<count($breadcrumbArray); $i++) {
				foreach($breadcrumbArray[$i] as $key => $value) {
					if ($i<(count($breadcrumbArray)-1)) {
						echo "<a href='".$value."'>".$key."</a>";
					} else {
						echo "<span class='current-page'>".$key."</span>";
					}
				}
				if ($i<(count($breadcrumbArray)-1)) {
					echo " / ";
				}
			}
		?>
		</div>


